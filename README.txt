Authenticated Entity Access
---------------------------
This module allows a site's content staff to limit visibility of content to
only visitors that are logged in, anonymous visitors will receive a 403 error.

The module leverages a simple checkbox field to determine which field is used to
control access.


Features
--------------------------------------------------------------------------------
The primary features include:

* An administration interface to manage which content types are supported.

* Leverages existing checkbox (boolean) fields, or allows creation of a new
  field directly from the admin page.

* All changes are handled as normal site configuration, allowing the changes to
  be deployed as part of a standard Drupal development process.


Standard usage scenario
--------------------------------------------------------------------------------
1. Install the module.
2. Open admin/config/content/entity_access.
3. Enable the option for any content types that need it, then either select an
   existing field or use the "Create new field named 'Restrict content'"
   option to automatically create a field named "Restrict content" (machine name
   "field_is_restricted") on that content type.


Credits / contact
--------------------------------------------------------------------------------
Currently maintained by Damien McKenna [1] and Rob Powell [2].

Ongoing development is sponsored by Mediacurrent [3].

The best way to contact the authors is to submit an issue, be it a support
request, a feature request or a bug report, in the project issue queue:
  https://www.drupal.org/project/auth_entity_access


References
--------------------------------------------------------------------------------
1: https://www.drupal.org/u/damienmckenna
2: https://www.drupal.org/u/robpowell
3: https://www.mediacurrent.com
