<?php

namespace Drupal\auth_entity_access\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\node\Entity\NodeType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;

/**
 * Returns responses for Honeypot module routes.
 */
class AuthEntityAccessSettingsForm extends ConfigFormBase {

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $displayRepository;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Name of the config bin.
   *
   * @var string
   */
  const AUTH_ENTITY_ACCESS_CONFIG_NAME = 'auth_entity_access.settings';

  /**
   * Name of config settings property.
   *
   * @var string
   */
  const AUTH_ENTITY_ACCESS_CONFIG_SETTINGS = 'settings';

  /**
   * Name of the auth_access field.
   *
   * @var string
   */
  const AUTH_ENTITY_ACCESS_FIELD_NAME = 'field_is_restricted';

  /**
   * Constructs a AuthEntityAccessSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler, EntityDisplayRepositoryInterface $entity_display_repository, EntityFieldManagerInterface $entity_field_manager) {
    parent::__construct($config_factory);
    $this->moduleHandler = $module_handler;
    $this->displayRepository = $entity_display_repository;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('entity_display.repository'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [static::AUTH_ENTITY_ACCESS_CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'auth_entity_access_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // @todo Add support for other content entity types.
    if ($this->moduleHandler->moduleExists('node')) {
      $types = NodeType::loadMultiple();
      if (!empty($types)) {
        $config_settings = static::AUTH_ENTITY_ACCESS_CONFIG_SETTINGS;
        $settings = $this
          ->config(static::AUTH_ENTITY_ACCESS_CONFIG_NAME)
          ->get($config_settings);

        $form[$config_settings] = [
          '#type' => 'table',
          '#caption' => [
            '#markup' => '<h2>' . $this->t('Content') . '</h2>',
          ],
          '#header' => [
            $this->t('Status'),
            $this->t('Name'),
            $this->t('Field'),
          ],
        ];

        foreach ($types as $type) {
          $bundle = $type->get('type');
          $id = 'node__' . $bundle;

          $form[$config_settings][$id]['status'] = [
            '#type' => 'checkbox',
            '#default_value' => $settings[$id]['status'],
          ];

          $form[$config_settings][$id]['bundle'] = [
            '#markup' => $this->t('@name', ['@name' => $type->label()]),
          ];

          $form[$config_settings][$id]['field'] = [
            '#type' => 'select',
            '#options' => [
              'create' => $this->t('Create new field named "Restrict content"'),
            ] + $this->getBooleanFields('node', $bundle),
            '#default_value' => $settings[$id]['field'],
            '#states' => [
              'visible' => [
                ':input[name="settings[' . $id . '][status]"]' => [
                  ['checked' => TRUE],
                ],
              ],
            ],
          ];
        }
      }
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(static::AUTH_ENTITY_ACCESS_CONFIG_NAME);

    // Save the forms from $form_state into a 'settings' array.
    $form_setting_values = $form_state
      ->getValue(static::AUTH_ENTITY_ACCESS_CONFIG_SETTINGS);

    foreach ($form_setting_values as $key => $value) {
      list($entity_type, $bundle) = explode('__', $key);
      if ($value['status'] && $value['field'] == 'create') {
        $this->addIsRestrictedField($entity_type, $bundle);
        $form_setting_values[$key]['field'] = static::AUTH_ENTITY_ACCESS_FIELD_NAME;
      }
    }

    $config->set(static::AUTH_ENTITY_ACCESS_CONFIG_SETTINGS, $form_setting_values);
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * Adds the restricted field to a node type.
   *
   * @param string $entity_type
   *   The entity to add field.
   * @param string $bundle
   *   The bundle to add field to.
   *
   * @see node_add_body_field()
   */
  private function addIsRestrictedField($entity_type, $bundle) {
    if ($entity_type === 'node') {
      // Add or remove the field_restrict_to_auth_access field, as needed.
      $field_name = static::AUTH_ENTITY_ACCESS_FIELD_NAME;
      $field = FieldConfig::loadByName($entity_type, $bundle, $field_name);
      if (empty($field)) {
        // Get field storage.
        $field_storage = FieldStorageConfig::loadByName($entity_type, $field_name);
        if (empty($field_storage)) {
          $field_storage = FieldStorageConfig::create([
            'type' => 'boolean',
            'field_name' => $field_name,
            'entity_type' => $entity_type,
          ]);
          $field_storage->save();
        }

        // Add field to bundle.
        $field = FieldConfig::create([
          'field_storage' => $field_storage,
          'bundle' => $bundle,
          'label' => $this->t('Restrict content'),
          'description' => $this->t('If checked, this content will only be available to authenticated users.'),
        ]);
        $field->save();

        // Todo: Update with 8.8.x, https://www.drupal.org/node/2835616.
        // Assign widget settings for the default form mode.
        \Drupal::service('entity_display.repository')->getFormDisplay($entity_type, $bundle, 'default')
          ->setComponent(static::AUTH_ENTITY_ACCESS_FIELD_NAME, [
            'type' => 'boolean_checkbox',
          ])
          ->save();

        // Assign display settings for the 'default' and 'teaser' view modes.
        \Drupal::service('entity_display.repository')->getViewDisplay($entity_type, $bundle, 'default')
          ->setComponent(static::AUTH_ENTITY_ACCESS_FIELD_NAME, [
            'label' => 'inline',
            'type' => 'boolean',
          ])
          ->save();

        // The teaser view mode is created by the Standard profile and therefore
        // might not exist.
        $view_modes = $this->displayRepository->getViewModes($entity_type);
        if (isset($view_modes['teaser'])) {
          \Drupal::service('entity_display.repository')->getViewDisplay('node', $bundle, 'teaser')
            ->setComponent(static::AUTH_ENTITY_ACCESS_FIELD_NAME, [
              'label' => 'inline',
              'type' => 'boolean',
            ])
            ->save();
        }
      }
    }
  }

  /**
   * Gets a map of fields across bundles.
   *
   * @param string $entity_type
   *   The entity type id.
   * @param string $bundle
   *   The bundle.
   *
   * @return array
   *   An array of boolean fields for the bundle.
   */
  public function getBooleanFields($entity_type, $bundle) {
    $fields = [];
    $all_fields = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);
    foreach ($all_fields as $field_name => $field_info) {
      if ($field_info->getType() == 'boolean') {
        $fields[$field_name] = $field_info->getLabel();
      }
    }
    return $fields;
  }

}
